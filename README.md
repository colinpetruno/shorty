Shorty Docs
================

### Running Shorty

You can run the shorty web service and all dependencies by the following 
command:
```
$ bin/run/web
```


### Testing Shorty

You can run the full test suite via:
```
$ bin/test/web
```

### Bundling

Due to the docker setup, bundling normally needs to be adjusted a bit so that
the cached gems will be reloaded into the running container. To do this, 
Docker compose must first be brought down and the old volume removed. There is
an easy script that does everything for you
```
$ bin/bundle/web
```

### Console

To prevent having to remember docker commands, you can access the console
via these commends
```
$ bin/console/web
$ bin/console/redis
```


Shorty Challenge
================

## The Challenge

The challenge, if you choose to accept it, is to create a micro service to shorten urls, in the style that TinyURL and bit.ly made popular.

## Rules

1. The service must expose HTTP endpoints according to the definition below.
2. Use [docker](https://docs.docker.com/) and [docker compose](https://docs.docker.com/compose/overview/), the only command needed to run your project must be `docker-compose up` 
3. It must be well tested, it must also be possible to run the entire test suit with a single command from the directory of your repository.
4. The service must be versioned using git and submitted by making a Pull Request against this repository, git history **should** be meaningful.
5. You don't have to use a datastore, you can have all data in memory, but we'd be more impressed if you do use one.

## Tips

* Less is more, small is beautiful, you know the drill — stick to the requirements.
* Use the right tool for the job, rails is highly discouraged.
* Don't try to make the microservice play well with others, the system is all yours.
* No need to take care of domains, that's for a reverse proxy to handle.
* Unit tests > Integration tests, but be careful with untested parts of the system.

**Good Luck!** — not that you need any ;)

-------------------------------------------------------------------------

## API Documentation

**All responses must be encoded in JSON and have the appropriate Content-Type header**


### POST /shorten

```
POST /shorten
Content-Type: "application/json"

{
  "url": "http://example.com",
  "shortcode": "example"
}
```

Attribute | Description
--------- | -----------
**url**   | url to shorten
shortcode | preferential shortcode

##### Returns:

```
201 Created
Content-Type: "application/json"

{
  "shortcode": :shortcode
}
```

A random shortcode is generated if none is requested, the generated short code has exactly 6 alpahnumeric characters and passes the following regexp: ```^[0-9a-zA-Z_]{6}$```.

##### Errors:

Error | Description
----- | ------------
400   | ```url``` is not present
409   | The the desired shortcode is already in use. **Shortcodes are case-sensitive**.
422   | The shortcode fails to meet the following regexp: ```^[0-9a-zA-Z_]{4,}$```.


### GET /:shortcode

```
GET /:shortcode
Content-Type: "application/json"
```

Attribute      | Description
-------------- | -----------
**shortcode**  | url encoded shortcode

##### Returns

**302** response with the location header pointing to the shortened URL

```
HTTP/1.1 302 Found
Location: http://www.example.com
```

##### Errors

Error | Description
----- | ------------
404   | The ```shortcode``` cannot be found in the system

### GET /:shortcode/stats

```
GET /:shortcode/stats
Content-Type: "application/json"
```

Attribute      | Description
-------------- | -----------
**shortcode**  | url encoded shortcode

##### Returns

```
200 OK
Content-Type: "application/json"

{
  "startDate": "2012-04-23T18:25:43.511Z",
  "lastSeenDate": "2012-04-23T18:25:43.511Z",
  "redirectCount": 1
}
```

Attribute         | Description
--------------    | -----------
**startDate**     | date when the url was encoded, conformant to [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
**redirectCount** | number of times the endpoint ```GET /shortcode``` was called
lastSeenDate      | date of the last time the a redirect was issued, not present if ```redirectCount == 0```

##### Errors

Error | Description
----- | ------------
404   | The ```shortcode``` cannot be found in the system

<<<<<<< HEAD

=======
>>>>>>> 97edab5... Initial Application Setup & Routing
