module Shorty
  class Application
    def call(env)
      status, headers, body = Shorty::Router.route(
        Rack::Request.new(env)
      )

      return [status, headers, body.map(&:to_s)]
    end
  end
end

Dir["./shorty/**/*.rb"].each {|file| require file }

run Shorty::Application.new