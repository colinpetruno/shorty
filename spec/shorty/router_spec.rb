require "spec_helper"

RSpec.describe Shorty::Router do
  describe ".route" do
    it "should instantiate the router and call the `route` method" do
      class RouterMock
        def initialize(request=nil)
        end

        def route
        end
      end

      request = rack_request_mock(
        "/aBc123/stats",
        "GET"
      )

      router_mock = RouterMock.new

      expect(router_mock).to receive(:route)

      expect(Shorty::Router).
        to receive(:new).
        with(request).
        and_return(router_mock)

      Shorty::Router.route(request)
    end
  end

  describe "#route" do
    context "when given an supported HTTP verb" do
      it "will respond with a bad request" do
        request = rack_request_mock("/shorten/", "PUT")

        status, headers, body = Shorty::Router.new(request).route

        expect(status).to eql(400)
      end
    end

    context "/shorten" do
      it "should still work with a trailing slash" do
        request = rack_request_mock(
          "/shorten/",
          "POST",
          nil,
          {
            "url": "http://example.com",
            "shortcode": "example"
          }
        )

        status, headers, body = Shorty::Router.new(request).route

        expect(status).to eql(201)
      end
    end


    context "/:short_code" do
      it "should still work with a trailing slash" do
        Shorty::ShortCodes::Creator.create(
          "http://example.com",
          "aBc123"
        )

        request = rack_request_mock(
          "/aBc123/",
          "GET"
        )

        status, headers, body = Shorty::Router.new(request).route

        expect(status).to eql(302)
      end
    end


    context "/:short_code/stats" do
      it "should not matter the case of the word stats" do
        Shorty::ShortCodes::Creator.create(
          "http://example.com",
          "aBc123"
        )

        request = rack_request_mock(
          "/aBc123/sTaTs/",
          "GET"
        )

        status, headers, body = Shorty::Router.new(request).route

        expect(status).to eql(200)
      end

      it "should still work with a trailing slash" do
        Shorty::ShortCodes::Creator.create(
          "http://example.com",
          "aBc123"
        )

        request = rack_request_mock(
          "/aBc123/stats/",
          "GET"
        )

        status, headers, body = Shorty::Router.new(request).route

        expect(status).to eql(200)
      end
    end

    context "for any other url" do
      it "should return a 404" do
        request = rack_request_mock(
          "/bad",
          "GET"
        )

        status, headers, body = Shorty::Router.new(request).route

        expect(status).to eql(404)
      end
    end
  end
end
