require "spec_helper"
require "timecop"

RSpec.describe Shorty::ShortCodes::ViewIncrementer do
  describe "#increment" do
    it "should increment the redirectCount" do
      Shorty::ShortCodes::Creator.create("http://example.com", "example")

      short_code_info = ::Shorty::ShortCodes::Finder.find("example")

      expect(short_code_info["redirectCount"].to_i).to eql(0)

      ::Shorty::ShortCodes::ViewIncrementer.increment("example")

      short_code_info = ::Shorty::ShortCodes::Finder.find("example")
      expect(short_code_info["redirectCount"].to_i).to eql(1)
    end


    it "should update the last seen date" do
      Timecop.freeze

      Shorty::ShortCodes::Creator.create("http://example.com", "example")

      short_code_info = ::Shorty::ShortCodes::Finder.find("example")

      expect(short_code_info["lastSeenDate"]).to eql("")

      ::Shorty::ShortCodes::ViewIncrementer.increment("example")

      short_code_info = ::Shorty::ShortCodes::Finder.find("example")
      expect(short_code_info["lastSeenDate"]).to eql(DateTime.now.iso8601)

      Timecop.return
    end
  end
end
