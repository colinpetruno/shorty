require "spec_helper"

RSpec.describe Shorty::ShortCodes::Creator do
  describe "#create" do
    it "should raise an error if the url is invalid" do
      expect(Shorty::ShortCodes::UrlValidator).
        to receive(:validate).
        and_raise(Shorty::ShortCodes::UrlMissingError.new(
          "url is not present"
        ))

      expect {
        Shorty::ShortCodes::Creator.create(
          "badurl",
          "example"
        )
      }.to raise_error(Shorty::ShortCodes::UrlMissingError)
    end

    it "should raise an error if the short code is taken" do
      Shorty::ShortCodes::Creator.create(
        "http://example.com",
        "example"
      )

      expect {
        Shorty::ShortCodes::Creator.create(
          "http://example.com",
          "example"
        )
      }.to raise_error(Shorty::ShortCodes::ShortCodeAlreadyExistsError)
    end

    it "should raise an error if the short code is invalid" do
      expect {
        Shorty::ShortCodes::Creator.create(
          "http://example.com",
          "3%s4ab"
        )
      }.to raise_error(Shorty::ShortCodes::InvalidShortCodeError)
    end

    it "should respond with the short code upon creation" do
      result = Shorty::ShortCodes::Creator.create(
        "http://example.com",
        "example"
      )

      expect(result =~ Shorty::ShortCodes::Creator::CUSTOM_SHORT_CODE_REGEX).
        to_not be_nil
    end
  end
end
