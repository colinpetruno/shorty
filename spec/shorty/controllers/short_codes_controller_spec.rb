require "spec_helper"

RSpec.describe Shorty::Controllers::ShortCodesController do
  describe ".create" do

    context "with a valid request" do
      it "should return a 201 status code" do
        request = rack_request_mock(
          "/shorten", "POST", nil,
          {
            "url": "http://example.com",
            "shortcode": "example"
          }
        )

        status, headers, body = Shorty::Controllers::ShortCodesController.
          new(request).
          create

        expect(status).to eql(201)
        expect(headers["Content-Type"]).to eql("application/json")
        expect(body).to eql([{ short_code: "example" }])
      end
    end

    context "with a bad url" do
      it "should give a 400 response code" do
        request = rack_request_mock(
          "/shorten", "POST", nil,
          {
            "url": "http://kjakjfoij390029jkk",
            "shortcode": "example"
          }
        )

        expect(Shorty::ShortCodes::UrlValidator).
          to receive(:validate).
          and_raise(Shorty::ShortCodes::UrlMissingError.new(
            "url is not present"
          ))

        status, headers, body = Shorty::Controllers::ShortCodesController.
          new(request).
          create

        expect(status).to eql(400)
        expect(headers["Content-Type"]).to eql("application/json")
        expect(body.first.keys.include?(:error)).to eql(true)
      end
    end

    context "with a taken shortcode" do
      it "should respond with a 409 status" do
        Shorty::ShortCodes::Creator.create("http://example.com", "example")

        request = rack_request_mock(
          "/shorten", "POST", nil,
          {
            "url": "http://example.com",
            "shortcode": "example"
          }
        )

        status, headers, body = Shorty::Controllers::ShortCodesController.
          new(request).
          create

        expect(status).to eql(409)
        expect(headers["Content-Type"]).to eql("application/json")
        expect(body).to eql([{ error: "The requested shortcode already exists" }])
      end
    end

    context "with an invalid shortcode" do
      it "should respond with a 422 status" do
        request = rack_request_mock(
          "/shorten", "POST", nil,
          {
            "url": "http://example.com",
            "shortcode": "ex1"
          }
        )

        status, headers, body = Shorty::Controllers::ShortCodesController.
          new(request).
          create

        expect(status).to eql(422)
        expect(headers["Content-Type"]).to eql("application/json")
        expect(body).to eql([{
          error: "The shortcode does not match '^[0-9a-zA-Z_]{4,}$'"
        }])
      end
    end
  end

  describe ".show" do
    it "should return a 302 redirect if it is found" do
      uris = ["/aBc123", "/a234", "/this_is_a_long_string"]

      uris.each do |uri|
        Shorty::ShortCodes::Creator.create(
          "http://example.com",
          uri.gsub("/", "")
        )

        request = rack_request_mock(
          uri,
          "GET"
        )

        status, headers, body = Shorty::Controllers::ShortCodesController.
          new(request).
          show

        expect(status).to eql(302)
        expect(headers["Location"]).to eql("http://example.com")
      end
    end

    it "should return a 404 status code if not found" do
        request = rack_request_mock(
          "/notarealkey",
          "GET"
        )

        status, headers, body = Shorty::Controllers::ShortCodesController.
          new(request).
          show

        expect(status).to eql(404)
        expect(headers["Content-Type"]).to eql("application/json")
        expect(body).to eql([{
          error: "The shortcode cannot be found in the system",
        }])
    end

    it "should be case sensitive" do
      Shorty::ShortCodes::Creator.create(
        "http://example.com",
        "asdf"
      )

      Shorty::ShortCodes::Creator.create(
        "http://google.com",
        "Asdf"
      )

      request = rack_request_mock(
        "/asdf",
        "GET"
      )

      status, headers, body = Shorty::Controllers::ShortCodesController.
        new(request).
        show

      expect(headers["Location"]).to eql("http://example.com")

      request = rack_request_mock(
        "/Asdf",
        "GET"
      )

      status, headers, body = Shorty::Controllers::ShortCodesController.
        new(request).
        show

      expect(headers["Location"]).to eql("http://google.com")
    end
  end
end
