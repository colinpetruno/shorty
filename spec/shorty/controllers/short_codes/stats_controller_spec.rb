require "spec_helper"
require "timecop"

RSpec.describe Shorty::Controllers::ShortCodes::StatsController do
  CONTROLLER = Shorty::Controllers::ShortCodes::StatsController

  describe ".show" do
    it "should return a 200 status code on a valid request" do
      Timecop.freeze

      Shorty::ShortCodes::Creator.create(
        "http://example.com",
        "example"
      )

      request = rack_request_mock(
        "/example/stats",
        "GET"
      )

      status, headers, body = CONTROLLER.new(request).show

      expect(status).to eql(200)
      expect(headers["Content-Type"]).to eql("application/json")
      expect(body).to eql([{
        "lastSeenDate" => "",
        "redirectCount" => "0",
        "startDate" => DateTime.now.iso8601,
        "url" => "http://example.com"
      }])

      Timecop.return
    end

    it "should return a 404 status code if not found" do
      request = rack_request_mock(
        "/example/stats",
        "GET"
      )

      status, headers, body = CONTROLLER.new(request).show

      expect(status).to eql(404)
      expect(headers["Content-Type"]).to eql("application/json")
      expect(body).to eql([{
        error: "The shortcode cannot be found in the system",
      }])
    end
  end
end
