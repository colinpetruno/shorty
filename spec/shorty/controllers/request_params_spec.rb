require "spec_helper"

RSpec.describe Shorty::Controllers::RequestParams do
  describe "#generate" do
    it "should add the body params and url params together" do
      # NOTE: This is a bit weird admittedly but has to do with some
      # differences between the Rack::RequestMock and the Rack::Request
      # classes. The Rack::RequestMock does not merge the POST body and url
      # params like the Request object does. Thus by quickly monkey patching
      # the GET to a POST tricks the RequestMock into combining the input from
      # the URL as well as the body. The only side negative side effects is a
      # bad merge key in the params object. Due to this, the expected array
      # and output array is intersected since 1 extra key is expected and then
      # checked for equalty to ensure existence of all the params. It's not
      # pretty but let's us at least get some confirmation that our param
      # object is working properly.
      Rack::GET = "POST"

      request = rack_request_mock(
        "/aBc123/stats",
        "POST",
        { page: 1 },
        { url: "http://example.com", shortcode: "example" }
      )

      params = Shorty::Controllers::RequestParams.new(request).generate

      Rack::GET = "GET"

      expect(
        (params.keys.map(&:to_sym) & [:page, :shortcode, :url]).sort
      ).to eql([:page, :shortcode, :url])
    end

    it "should handle invalid json request bodies" do
    end
  end
end
