# Shorty Testing Information

The application is based on rack and provides two helpers to assist in 
writing your tests.

## Running tests

`bundle exec rspec spec/`

### Request Object Mock

The request object mock is a ruby wrapper around racks environment hash. This
makes it easy to access various hard to remember hash keys with simple methods
that better reflect http terminology. 

**rack\_request\_mock(path="/", method="GET", params={}, body={})**

- **path:** The path of the url you want to hit
- **method:** The http request method, supported options are: (GET | POST)
- **params:** Url based parameters
- **body:** The hash form of the key values in the body. 


### Request Env Mock

The request env mock method will build a hash of the rack request env. This is
important to use because the body of a rack env is a string scanner and not a
normal string. This means after reading the body you will need to rewind the
string back to the start point if you want to use it again. Utilizing a string
scanner allows one to handle large requests without a large memory footprint.

[Read more on String Scanner](https://ruby-doc.org/stdlib-2.5.3/libdoc/strscan/rdoc/StringScanner.html)

This method is also a bit more generic and flexible. This is a simple service
and since everything is json, the request mock is more limited and would need
expanded to support applications utilizing more of the HTTP API. 


**rack\_env\_mock(path="/", method="GET", params={}, body={})**

- **path:** The path of the url you want to hit
- **method:** The http request method, supported options are: (GET | POST)
- **params:** Url based parameters
- **body:** The hash form of the key values in the body. 


