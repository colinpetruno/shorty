require "rack"
require "json"

def rack_request_mock(path="/", method="GET", params={}, body={})
  Rack::Request.new(
    rack_env_mock(path, method, params, body.to_json)
  )
end

def rack_env_mock(path="/", method="GET", params={}, body=nil)
  Rack::MockRequest.env_for(
    path,
    {
      input: body,
      method: method,
      params: params
    }
  )
end
