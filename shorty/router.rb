module Shorty
  class Router
    def self.route(request)
      new(request).route
    end

    def initialize(request)
      @request = request
    end

    def route
      if request.get?
        find_get_route
      elsif request.post?
        find_post_route
      else
        [400, { "Content-Type" => "text/plain" }, ["400: Bad Request."]]
      end
    end

    private

    attr_reader :request

    def find_get_route
      case request.path
        when /^\/[0-9a-zA-Z_]{4,}$|^\/[0-9a-zA-Z_]{4,}\/$/
          # GET /:shortcode | /:shortcode/
          ::Shorty::Controllers::ShortCodesController.new(request).show
        when /^\/[0-9a-zA-Z_]{4,}\/(?i:stats)$|^\/[0-9a-zA-Z_]{4,}\/(?i:stats)\/$/
          # GET /:shortcode/stats | /:shortcode/STATS
          ::Shorty::Controllers::ShortCodes::StatsController.new(request).show
        else
          [404, { "Content-Type" => "text/plain" }, ["404: Not Found."]]
      end
    end

    def find_post_route
      case request.path
        when /^\/shorten$|^\/shorten\/$/i
          # GET /shorten | /shorten/ | /SHORTEN/
          ::Shorty::Controllers::ShortCodesController.new(request).create
        else
          [404, { "Content-Type" => "text/plain" }, ["404: Not Found."]]
      end
    end

    def get_request?
      request.method == "GET"
    end
  end
end
