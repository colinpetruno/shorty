require "securerandom"
require "date"

module Shorty::ShortCodes
  class InvalidShortCodeError < StandardError; end;
  class UrlMissingError < StandardError; end;
  class ShortCodeAlreadyExistsError < StandardError; end;

  class Creator
    CUSTOM_SHORT_CODE_REGEX = /^[0-9a-zA-Z_]{4,}$/

    def self.create(url, custom_short_code = nil)
      new(url, custom_short_code).create
    end

    def initialize(url, custom_short_code=nil)
      @custom_short_code = custom_short_code
      @url = url

      validate_custom_short_code if custom_short_code != nil
      validate_url
    end

    def create
      result = redis.hsetnx(short_code, "url", url)

      if result == false
        raise ShortCodeAlreadyExistsError.new(
          "The requested shortcode already exists"
        )
      end

      redis.hset(
        short_code,
        "redirectCount", 0,
        "startDate", DateTime.now.iso8601,
        "lastSeenDate", nil
      )

      return short_code
    end

    private

    attr_reader :custom_short_code, :url

    def validate_url
      ::Shorty::ShortCodes::UrlValidator.validate(url)
    end

    def validate_custom_short_code
      unless custom_short_code =~ CUSTOM_SHORT_CODE_REGEX
        raise InvalidShortCodeError.new(
          "The shortcode does not match '^[0-9a-zA-Z_]{4,}$'"
        )
      end
    end

    def short_code
      @_short_code ||= custom_short_code || generate_short_code
    end

    def generate_short_code
      SecureRandom.alphanumeric(6)
    end

    def redis
      ::Shorty::RedisServer::Connection.instance.connection
    end
  end
end
