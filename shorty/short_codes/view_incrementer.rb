module Shorty::ShortCodes
  class ViewIncrementer
    def self.increment(short_code)
      new(short_code).increment
    end

    def initialize(short_code)
      @short_code = short_code
    end

    def increment
      redis.pipelined do
        redis.hincrby(short_code, "redirectCount", 1)
        redis.hset(short_code, "lastSeenDate", DateTime.now.iso8601.to_s)
      end
    end

    private

    attr_reader :short_code

    def redis
      ::Shorty::RedisServer::Connection.instance.connection
    end
  end
end
