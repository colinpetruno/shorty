require "date"

module Shorty::ShortCodes
  class NotFoundError < StandardError; end;

  class Finder
    def self.find(short_code)
      new(short_code).find
    end

    def initialize(short_code)
      @short_code = short_code
    end

    def find
      short_code_details = redis.hgetall(short_code)

      if short_code_details.keys.length == 0
        raise NotFoundError.new("The shortcode cannot be found in the system")
      end

      return short_code_details
    end

    private

    attr_reader :short_code

    def redis
      ::Shorty::RedisServer::Connection.instance.connection
    end
  end
end
