require "open-uri"

module Shorty::ShortCodes
  class UrlValidator
    def self.validate(url)
      new(url).validate
    end

    def initialize(url)
      @url = url
    end

    def validate
      status, status_string = open(url).status

      unless  (200..399).include?(status.to_i)
        raise UrlMissingError.new(
          "We were unable to reach the requested website."
        )
      end

      return true
    rescue StandardError => error
      raise UrlMissingError.new(
        "url is not present"
      )
    end

    private

    attr_reader :url
  end
end
