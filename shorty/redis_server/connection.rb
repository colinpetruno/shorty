require "singleton"
require "redis"

module Shorty::RedisServer
  class Connection
    include ::Singleton

    def connection
      @_connection ||= ::Redis.new(url: "redis://redis:6379", db: database)
    end

    private

    def database
      # NOTE: Redis provides 15 numbered databases out of the box. We want to
      # ensure we can clear the tests after each test run
      if ENV["SHORTY_ENV"] == "test"
        3
      else
        2
      end
    end
  end
end
