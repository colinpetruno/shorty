module Shorty::Controllers
  class ShortCodesController < BaseController
    def create
      @short_code = ::Shorty::ShortCodes::Creator.create(
        params["url"],
        params["shortcode"]
      )

      return [
        201,
        { "Content-Type" => "application/json" },
        [{ short_code: @short_code }]
      ]

    rescue Shorty::ShortCodes::InvalidShortCodeError => error
      return error_response(422, error)
    rescue Shorty::ShortCodes::UrlMissingError => error
      return error_response(400, error)
    rescue Shorty::ShortCodes::ShortCodeAlreadyExistsError => error
      return error_response(409, error)
    end

    def show
      @short_code_details = Shorty::ShortCodes::Finder.find(short_code_from_url)

      Shorty::ShortCodes::ViewIncrementer.increment(short_code_from_url)

      [
        302,
        { "Location" => @short_code_details["url"] },
        []
      ]
    rescue Shorty::ShortCodes::NotFoundError => error
      error_response(404, error)
    end
  end
end
