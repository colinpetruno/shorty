require "json"

module Shorty::Controllers
  class RequestParams
    def self.for(request)
      new(request).generate
    end

    def initialize(request)
      @request = request
    end

    def generate
      body_params.merge(url_params)
    end

    private

    attr_accessor :request

    def body_params
      @_body_params ||= read_body
    end

    def url_params
      request.params
    end

    def read_body
      parsed_body = JSON.parse(request.body.read)

      request.body.rewind

      parsed_body
    rescue JSON::ParserError
      {}
    end
  end
end
