module Shorty::Controllers
  class BaseController
    def initialize(request)
      @request = request
    end

    private

    attr_reader :request

    def params
      @_params ||= ::Shorty::Controllers::RequestParams.for(request)
    end

    def error_response(status_code, error)
      [
        status_code,
        { "Content-Type" => "application/json" },
        [{ error: error.message }]
      ]
    end

    def short_code_from_url
      request.path.split("/")[1]
    end
  end
end
