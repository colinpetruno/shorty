module Shorty::Controllers::ShortCodes
  class StatsController < ::Shorty::Controllers::BaseController
    def show
      @short_code_details = ::Shorty::ShortCodes::Finder.find(
        short_code_from_url
      )

      [200, { "Content-Type" => "application/json" }, [@short_code_details]]
    rescue Shorty::ShortCodes::NotFoundError => error
      error_response(404, error)
    end
  end
end
